'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['ui.bootstrap', 'myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    
    // view 1
    $routeProvider.when('/view1', {
    	templateUrl: 'partials/partial1.html', 
    	controller: MyCtrl1
    });
    
    //view 2
    $routeProvider.when('/view2/:vmi', {
    	templateUrl: 'partials/partial2.html', 
    	controller: MyCtrl2
    });

    //view 3
    $routeProvider.when('/view3', {
        templateUrl: 'partials/partial3.html', 
        controller: MyCtrl3
    });
    
    // else
    $routeProvider.otherwise({redirectTo: '/view1'});

  }]);

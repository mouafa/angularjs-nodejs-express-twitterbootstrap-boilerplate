'use strict';

/* Controllers */


function MyCtrl1($scope, Phone) {
	$scope.dynamicTooltip = "It's just angular :)";

	// from Phone service
  	Phone.query({ test: 'test'}, function(data) {
  		$scope.phones = data['phones'];
  	});
}


function ModalDemoCtrl($scope) {
	$scope.closeMsg = "never closed";

	$scope.open = function () {
		$scope.shouldBeOpen = true;
	};

	$scope.close = function () {
		$scope.closeMsg = 'I was closed at: ' + new Date();
		$scope.shouldBeOpen = false;
	};
};

function MyCtrl2($scope, $routeParams) {
	$scope.alerts = [];
	angular.forEach($routeParams, function(value, key){
		$scope.alerts.push({
			type: 'success',
			msg: 'routeParams: ' + value
		});
	});

	$scope.addAlert = function() {
    	$scope.alerts.push({msg: "Another alert!"});
  	};

	$scope.closeAlert = function(index) {
    	$scope.alerts.splice(index, 1);
	};
}

// MyCtrl3
function MyCtrl3($scope) {
	$scope.rows = []
	for(var i = 0; i<20; i++) {
		$scope.rows[i] = {
			name: 'name: ' + i
		};
	}
}
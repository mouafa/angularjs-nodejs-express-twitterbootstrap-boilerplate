'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', ['ngResource'])

	// version
	.factory('version', function() {
		return '0.1';
	})

	// Phone
	.factory('Phone', function($resource){
  		return $resource('api/phones/', {test: 'defvalue'}, {
    		query: {
    			method:'GET', 

    			// extra param
    			params:{
    				testExtra: 'test2'
    			}
    		}, 
    		isArray:true
    	})
 	});
exports.index = function(req, res){
	res.render('index');
};

exports.phones = function(req, res){
	// console.log(req.param('test'));

	res.json({
		phones: [
			{
				name: 'iPhone'
			}, 
			{
				name: 'HTC'
			}, 
			{
				name: 'Samsung'
			}
		]
	});
};
